=============================
Application developer's guide
=============================

-----------------------------
Implementing blockchain logic
-----------------------------

In the most typical case, to implement a custom blockchain you only need to implement GTX operations and a module, which can then be plugged into the rest of the system.

This can be done in three steps:

1. Define the schema in an SQL file. It should contain defined tables and stored procedures needed by the application.
2. Define GTX operations by subclassing net.postchain.gtx.GTXOperation.
3. Define the GTX module which maps names to operations, by subclassing net.postchain.gtx.GTXModule interface, or by subclassing net.postchain.gtx.SimpleGTXModule.

* Optionally, if GTX module needs parameters, one can define GTX module factory (net.postchain.gtx.GTXModuleFactory).

GTXTestOp is an example of a simple GTX operation which runs an INSERT query with a user-provided string. It is included into GTXTestModule which includes schema in an inline string and also includes an example of a query implementation.

A more complex example can be found in the `tutorial <https://bitbucket.org/chromawallet/postchain-client/src/master/>`_.

If it is necessary to use a custom transaction format, it can be done in following way:

1. Implement net.postchain.core.Transaction interface to define transaction serialization format and semantics.
2. Implement transaction factory (net.postchain.core.TransactionFactory), usually it just calls transaction constructor.
3. Implement BlockchhainConfiguration. Simplest way is to subclass net.postchain.BaseBlockchainConfiguration and override getTransactionFactory method.
4. Implement BlockchainConfigurationFactory.

Steps 2-4 are usually trivial.
