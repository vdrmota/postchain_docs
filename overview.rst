========
Overview
========

Public/private blockchains
^^^^^^^^^^^^^^^^^^^^^^^^^^

If we take a Bitcoin as a primary example of a blockchain, there are a number of properties which are specific to cryptocurrencies:

* Fixed supply
* Censorship resistance
* No counterparty risk
* No intermediaries (in transactions)

but these features aren't relevant in broader enterprise contexts. On the other hand, the following features might be relevant:

* Decentralization (no single entity is in control)
* Byzantine fault tolerance (failure of a fraction of nodes does not jeopardise the system)
* End-to-end cryptographic authentication of transactions

Note that properties listed above overlap, and can be also seen as different facets of a single feature: fault-tolerant multiparty data sharing.

Problems with existing blockchains
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Blockchains usually rely on key-value data stores.
* Indexing and history tracking is often done through external processes which increases complexity
* If you have data in two places, which of them do you trust?

Ethereum as a data store
^^^^^^^^^^^^^^^^^^^^^^^^

Ethereum offers two data retrieval APIs:

* Object-oriented API through Solidity constant functions
* Index access through web3 & RPC APIs

Both are significantly less convenient and powerful than the relational queries offered by a conventional DBMS.

Consortium database
^^^^^^^^^^^^^^^^^^^

If the blockchain manages data, it should do it properly. This means:

* A rich data model
* History tracking
* Indexed access
* Flexible queries
* Transactions
* Constraints (security)

Many of these capabilities are features of a relational database. We asked ourselves:

What if we could use a relational database as a
blockchain data store **AND** Define blockchain logic using SQL stored procedures? 

Postchain is the result of this line of inquiry. It can be understood as components which allow one to implement blockchain-like synchronization on top of an ordinary SQL-hashed database.

In a postchain-based system clients cannot write data directly into a database using SQL queries. Instead, clients submit transactions to the system in form of signed messages which are sent to validator nodes. Validators check if a transaction is well-formed and is properly authorized before applying it.

All validators must apply transactions in the same order so that they all arrive to an identical database state. If every transaction is deterministic, then applying a sequence of transaction will result in the same final state on all nodes. Agreement on transaction order is also necessary to deal with conflicting transactions. Ordering will make sure that all nodes react to conflicting transactions in the same way.

Transactions are organized into blocks. A block is a sequence of transactions with synchronization-related metadata attached to it. Blocks are organized into a chain, as each block points to a previous block. An ordering of blocks in the chain together with an ordering of transactions within the block defines the ordering of transactions within a blockchain, which results in the same database state on every synchronized node. In other words, a blockchain is used for database replication.

It is important to highlight that all of the blockchain state must be in the database. Transaction-handling logic should never rely on data stored elsewhere. This is essentially the big idea about postchain: by keeping all the state in an SQL database, we can rely on its transaction isolation and other ACID properties. This greatly simplifies transaction-handling code as it doesn't need to bother itself with mempool or other implementation details, it should simply interact with the provided database connection. The transaction handling implementation essentially boils down to a single function which validates transactions in the context of the current state of the database and applies changes by performing SQL queries.

Consensus
^^^^^^^^^

Validators use a consensus algorithm to select client-submitted transactions into the next block. Transactions are committed to the database only after a super-majority of validator nodes agree on block contents.

A consensus algorithm is also used to synchronize the blockchain across nodes. By default Postchain uses EBFT [LINK] consensus, but can work with other consensus backends.

Postchain structure
^^^^^^^^^^^^^^^^^^^

Postchain does not require any particular format for transactions and blocks, or a specific consensus algorithm. At its core, Postchain merely defines interfaces of transaction and block components and the general architecture of the system, so different components can work together as long as they implement compatible interfaces. This enables code reuse. For example, new transaction handling logic (new database schema, queries, etc) can be combined with an existing block synchronization algorithm.

Transaction messages
^^^^^^^^^^^^^^^^^^^^

Postchain can work in transactions in any format. It is even possible to use message formats defined for other system, for example, Bitcoin transactions or XML documents used in enterprise applications. 

