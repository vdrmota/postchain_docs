================
Deployment guide
================

Running Postchain nodes
^^^^^^^^^^^^^^^^^^^^^^^

To set up Postchain network, the following steps are needed:

1. Prepare jar files which implement blockchain application.
2. Define the common node configuration, which includes the following:
    a. blockchain identifier (blockchainrid)
    b. blockchain configuration and module classes
    c. module parameters (if needed)
    d. public keys used by nodes
    e. node network addresses
    f. Define per-node configuration which includes
        i) database URL (host, username, password), schema
        ii) client API port
        iii) private key
    g. Configure database (usually creating database and user is enough)
    h. Run Postchain nodes with given configuration on each machine
    
    
-------------------------------
Example of common configuration
-------------------------------
*Note that this example includes common.properties.*

At least 4 nodes are needed for a fault-tolerant configuration, but it's possible to run network with just 3 nodes.

For development purposes it might be convenient to run Postchain in a single-node mode, in this case it can be configured like this

Postchain node can be launched like this::

    java -jar postchain.jar -i 2 -c node2.properties

Parameter -i specifies node index starting from 0, parameter -c specifies node's configuration file.

Here's an example of module parameters used for FT module::

    blockchain.1.gtx.modules=net.postchain.modules.ft.BaseFTModuleFactory
    blockchain.1.gtx.ft.assets=USD
    blockchain.1.gtx.ft.asset.USD.issuers=03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8
    blockchain.1.gtx.ft.openRegistration=true

