.. postchain_docs documentation master file, created by
   sphinx-quickstart on Mon Sep 11 16:47:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========
Postchain
=========

Postchain is a modular framework for implementing custom blockchains. Particularly, it's geared towards consortium blockchains (also known as permissioned, enterprise, private, federated blockchains, and, sometimes, distributed ledger technology).

This model is also known as proof-of-authority, to contrast it with proof-of-work and proof-of-stake. A Postchain network consists of a number of nodes, each maintaining an identical set of data. The key point of difference between Postchain and other private blockchains is that it integrates with SQL databases in a very deep way: all blockchain data is stored in an SQL database, transaction logic can be defined in terms of SQL code (particularly, stored procedures). However we should note that Postchain uses SQL as a black box: it is not a database plugin and it works with databases such as PostgreSQL as is, without any special configuration or modification. SQL database tools meet the requirements of a blockchain (atomicity, determinism, rollback) while allowing for greater data complexity, easier deployment, and more secure integration with existing systems. 

A whitelist of transaction types defines the rules and capabilities of the system, and transactions which are valid according to the rules of the system are distributed to all nodes using a consensus method which ensures that all nodes arrive at an identical state as long as a majority of nodes are functioning correctly. Committed transactions are stored in a blockchain in the database; a cryptographically secured assertion of state which is difficult to modify, and can be used to reconstruct the entire state of the database.

Contents
========

.. toctree::
   :maxdepth: 2

    Overview <overview.rst>
    Architecture <architecture.rst>
    Postchain-ft <postchain-ft.rst>
    Security <security.rst>
    Software requirements <software-requirements.rst>
    Application developer's guide [TBD] <development.rst>
    Deployment guide [TBD] <deployment.rst>


Search
^^^^^^

* :ref:`search`
